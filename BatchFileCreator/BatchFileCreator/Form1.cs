﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BatchFileCreator
{
    public partial class Form1 : Form
    {
        private string defaultImageFolder = @"C:\CTCReviewerLog";
        private string defaultScriptFileFolder = @"C:\CTCReviewerLog";
        private List<SampleDataItem> samples = new List<SampleDataItem>();
        const string LEICA_RED_FILEPREFIX = "TRITC-Rhoadmine Selection";
        const string ZEISS_RED_FILEPOSTFIX1 = "_c3_ORG.tif";
        const string ZEISS_RED_FILEPOSTFIX2 = "_c2_ORG.tif";

        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddSample_Click(object sender, EventArgs e)
        {
            OpenFileDialog openImageFile = new OpenFileDialog();
            openImageFile.Filter = "Red Channel TIFF File (*.tif) | *.tif";
            openImageFile.InitialDirectory = defaultImageFolder;
            if (openImageFile.ShowDialog() == DialogResult.OK)
            {
                string ImageFileName = openImageFile.SafeFileName;
                if ((ImageFileName.IndexOf(LEICA_RED_FILEPREFIX) != -1) ||
                    (ImageFileName.IndexOf(ZEISS_RED_FILEPOSTFIX1) != -1) ||
                    (ImageFileName.IndexOf(ZEISS_RED_FILEPOSTFIX2) != -1))
                {
                    string ImagePathname = openImageFile.FileName;
                    int index = ImagePathname.LastIndexOf('\\');
                    ImagePathname = ImagePathname.Substring(0, index);
                    defaultImageFolder = ImagePathname;
                    string[] itemText = new string[2];
                    itemText[0] = ImagePathname;
                    itemText[1] = openImageFile.SafeFileName;
                    ListViewItem item = new ListViewItem(itemText);
                    SampleList.Items.Add(item);
                    SampleDataItem dataItem = new SampleDataItem();
                    dataItem.m_ImageFolderName = ImagePathname;
                    dataItem.m_ImageFileName = openImageFile.SafeFileName;
                    samples.Add(dataItem);
                }
                else
                {
                    MessageBox.Show("Image File does not have Leica Red Channel TIFF File Prefix TRITC-Rhoadmine Selection or Zeiss Postfix _c3_ORG.tif or _c2_ORG.tif");
                }
            }
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Batch Script File (*.txt) | *.txt";
            dialog.InitialDirectory = defaultScriptFileFolder;
            dialog.DefaultExt = "*.txt";
            dialog.FileName = "CellRegionFinderBatch";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                SaveScript(dialog.FileName);
                string ScriptPathname = dialog.FileName;
                int index = ScriptPathname.LastIndexOf('\\');
                ScriptPathname = ScriptPathname.Substring(0, index);
                defaultScriptFileFolder = ScriptPathname;
            }
        }

        private void SaveScript(string filename)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                foreach (ListViewItem item in SampleList.Items)
                {
                    string line = item.SubItems[0].Text + "," + item.SubItems[1].Text + ",";
                    file.WriteLine(line);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SampleList.View = View.Details;
            SampleList.GridLines = true;
            SampleList.FullRowSelect = true;

            SampleList.Columns.Add("Image Folder Name", 250);
            SampleList.Columns.Add("Red Channel TIFF Image File Name", 400);
        }

        private void SampleList_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewItem theClickedOne = SampleList.GetItemAt(e.X, e.Y);
            foreach (SampleDataItem item in samples)
            {
                if (item.m_ImageFolderName.Equals(theClickedOne.Text) || item.m_ImageFileName.Equals(theClickedOne.Text))
                {
                    samples.Remove(item);
                    break;
                }
            }

            SampleList.Items.Clear();
            string[] arr = new string[2];
            ListViewItem itm;
            foreach (SampleDataItem item in samples)
            {
                arr[0] = item.m_ImageFolderName;
                arr[1] = item.m_ImageFileName;
                itm = new ListViewItem(arr);
                SampleList.Items.Add(itm);
            }
        }
    }
}
